package second.sample.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import second.common.common.CommandMap;  //커맨드맵 먼저 만드셔요 모든전송용으로 커맨드맵 활용.
import second.sample.service.SampleService;    //샘플서비스 먼저 만드셔요

@Controller
public class SampleController { //컨트롤러 만들고
	Logger log = Logger.getLogger(this.getClass()); //로그 찍을건데
	
	@Resource(name="sampleService") //등록된 이름을 찾아쓴다.
	private SampleService sampleService; //주입받아쓰겠다는말
		
	//글목록 : 오픈보드리스트가 동작하는데(리스트 출력하는 것)
	@RequestMapping(value="/sample/openBoardList.do")
    public ModelAndView openBoardList(CommandMap commandMap) throws Exception{
    	ModelAndView mv = new ModelAndView("/sample/boardList");
    	
								//이걸 호출하고 맵에 던져줌
    	List<Map<String,Object>> list = sampleService.selectBoardList(commandMap.getMap());
    	mv.addObject("list", list);  //뷰단에서 쓸수 있도록 저장해놓고, 뷰단에선 가져다 출력만하면 끝.
    	
    	return mv;
    }
	//글쓰기
	@RequestMapping(value="/sample/openBoardWrite.do")
	//커맨드맵 받아서 리턴한다. (안받아도 되긴한데, 그냥 다 집어넣은것. 안쓰니까 지워도 됨)
	public ModelAndView openBoardWrite(CommandMap commandMap) throws Exception{
		ModelAndView mv = new ModelAndView("/sample/boardWrite");
		
		return mv;
	}
	
	//버튼 누르면 게시글입력 되는 로직 
	@RequestMapping(value="/sample/insertBoard.do") //바뀜3-1
	//커맨드맵에 고대로 저장되어 
	public ModelAndView insertBoard(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/sample/openBoardList.do");
		
		sampleService.insertBoard(commandMap.getMap(), request); //바뀜3-2
		///따라가보자. '샘플서비스'에 인서트보드 보면 (맵객체는 줬으니) => 'DAO 인서트보드'로 가는데
		/// => 상속해준 => 앱스트랙트까지 가면 쿼리 ID 를 찾아서 SQL.xml 까지 간다.
		return mv;
	}
	
	//게시글 상세보기 
	@RequestMapping(value="/sample/openBoardDetail.do")
	// 글번호가 항상 넘어가야하니, 맵(CommandMap commandMap)으로 받고
	public ModelAndView openBoardDetail(CommandMap commandMap) throws Exception{
		ModelAndView mv = new ModelAndView("/sample/boardDetail"); //2여기로 포워딩해 
		
		//1맵을 뽑아내고
		Map<String,Object> map = sampleService.selectBoardDetail(commandMap.getMap());
		mv.addObject("map", map.get("map")); //6줄 이것하고4-2바뀜
		mv.addObject("list", map.get("list"));  // 7줄 이것4-2바뀜  
		//맵과 리스트가 들어있다는걸 알 수 있는데, 서비스임플 과 DAO를 보면 해소될듯
				
		return mv; //2여기
	}

	//수정 폼 -> 있는데이터 뽑아서 수정할 수 있게 보여줌(띄움)
	@RequestMapping(value="/sample/openBoardUpdate.do")
	public ModelAndView openBoardUpdate(CommandMap commandMap) throws Exception{
		
		ModelAndView mv = new ModelAndView("/sample/boardUpdate"); //객체생성(String:주소)
		
		//(참고) 셀렉트보드디테일을 뽑아오기때문에, 조회수1 증가하는 현상이 나옴.
		Map<String,Object> map = sampleService.selectBoardDetail(commandMap.getMap());
		mv.addObject("map", map);
		
		return mv;
	}
	
	//수정하기(저장하기 버튼) 누르는동작 -> 
	@RequestMapping(value="/sample/updateBoard.do")
	public ModelAndView updateBoard(CommandMap commandMap) throws Exception{
		
		ModelAndView mv = new ModelAndView("redirect:/sample/openBoardDetail.do");
		
		sampleService.updateBoard(commandMap.getMap());
		
		mv.addObject("IDX", commandMap.get("IDX"));  //idx 가져다쓰게끔
		return mv;
	}

	//삭제하기 누르는 동작 (진정한 삭제는 아님 ) 
	@RequestMapping(value="/sample/deleteBoard.do")
	public ModelAndView deleteBoard(CommandMap commandMap) throws Exception{
		
		ModelAndView mv = new ModelAndView("redirect:/sample/openBoardList.do");
		
		sampleService.deleteBoard(commandMap.getMap());
		
		return mv;
	}
}