package second.sample.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import second.common.util.FileUtils;  //만든걸로 임포트
import second.sample.dao.SampleDAO;

@Service("sampleService") /// sampleService 라는 id로 저장되도록 Service어노테이션 
public class SampleServiceImpl implements SampleService {
	Logger log = Logger.getLogger(this.getClass()); //로그객체 생성
	
	@Resource(name="fileUtils") //바뀜3-5
	private FileUtils fileUtils; //직접 만든걸로 임포트
	
	@Resource(name="sampleDAO") //등록된 이름으로 찾아쓴다.
	private SampleDAO sampleDAO; // 임포트함, 주입받아 쓰겠다는 말
	
	
	@Override
	public List<Map<String, Object>> selectBoardList(Map<String, Object> map) throws Exception {
		return sampleDAO.selectBoardList(map);
	}

	/*
	 * @Override //바뀜3-4 (많이) 
	 * public void insertBoard(Map<String, Object> map, HttpServletRequest request ) throws Exception { 
	 * sampleDAO.insertBoard(map);
	  
	  MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request; 
	  Iterator<String> iterator = multipartHttpServletRequest.getFileNames(); 
	  MultipartFile multipartFile = null; 
	  while(iterator.hasNext()){ multipartFile = multipartHttpServletRequest.getFile(iterator.next());
	  if(multipartFile.isEmpty() == false){
	  log.debug("------------- file start -------------");
	  log.debug("name : "+multipartFile.getName());
	  log.debug("filename : "+multipartFile.getOriginalFilename());
	  log.debug("size : "+multipartFile.getSize());
	  log.debug("-------------- file end --------------\n"); } }
	  
	 * }
	 */
	
	@Override //바뀜3-6 인서트파일 메소드 (기존 3-4는 주석처리)
	public void insertBoard(Map<String, Object> map, HttpServletRequest request) throws Exception {
		sampleDAO.insertBoard(map);
		
		List<Map<String,Object>> list = fileUtils.parseInsertFileInfo(map, request);
		for(int i=0, size=list.size(); i<size; i++){
			sampleDAO.insertFile(list.get(i));
						
		}
	}
	
	

	@Override //상세보기
	public Map<String, Object> selectBoardDetail(Map<String, Object> map) throws Exception {
		sampleDAO.updateHitCnt(map);//조회수 +1 되도록 하고
		// Map<String, Object> resultMAP = sampleDAO.selectBoardDetail(map);//1줄 데이터 가져와서
		Map<String, Object> resultMap = new HashMap<String,Object>(); //( 4-3바뀜 ) 1
		
		Map<String, Object> tempMap = sampleDAO.selectBoardDetail(map); //2 이걸 꺼내서
		resultMap.put("map", tempMap); // 1 에다가 2 꺼낸걸 저장해준다.
		
		List<Map<String,Object>> list = sampleDAO.selectFileList(map); //이어서 DAO에서 신설할 메소드
		resultMap.put("list", list); //리스트를 담아서
		
	
		return resultMap; //한덩어리로 리턴한다
	}

	@Override
	public void updateBoard(Map<String, Object> map) throws Exception {
		sampleDAO.updateBoard(map);

	}

	@Override
	public void deleteBoard(Map<String, Object> map) throws Exception {
		sampleDAO.deleteBoard(map);

	}

}
