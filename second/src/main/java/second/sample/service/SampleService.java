package second.sample.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface SampleService {

	//목록은 여러줄이니 맵을 리스트로한번 더 구성한 후 리턴되도록
	List<Map<String,Object>> selectBoardList(Map<String, Object> map) throws Exception;
	
	//바뀜 3-3
	void insertBoard(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	//디테일은 1줄이니까 맵으로 리턴
	Map<String, Object> selectBoardDetail(Map<String, Object> map) throws Exception;
		
	void updateBoard(Map<String, Object> map) throws Exception;
	
	void deleteBoard(Map<String, Object> map) throws Exception;
		
	
}
