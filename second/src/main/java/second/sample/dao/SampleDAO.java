package second.sample.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import second.common.dao.AbstractDAO;

///이건 sampleDAO id로 컨테이너에 등록되도록 만들었다. 라는 의미 (스캔으로)
@Repository("sampleDAO")  //대소문자 오타주의
public class SampleDAO extends AbstractDAO{

	/// 맵객체를 받아서 AbstractDAO가 가지고 있는 셀렉트리스트를 받아서 쿼리id와 객체(map)을 전달
	@SuppressWarnings("unchecked") //셀렉트리스트
	public List<Map<String, Object>> selectBoardList(Map<String, Object> map) throws Exception{
		return (List<Map<String, Object>>)selectList("sample.selectBoardList", map);
	}	/// ㄴ맵객체를 리스트에 담아서 객체(map)을 전달. 
		///(셀렉트는 기본적으로 오브젝트를 리턴하기에 형변환)
	
	public void insertBoard(Map<String, Object> map) throws Exception{
		insert("sample.insertBoard", map);
	}

	public void updateHitCnt(Map<String, Object> map) throws Exception{
		update("sample.updateHitCnt", map);
	}

	@SuppressWarnings("unchecked")///상기 주석과 동일 //셀렉트원
	public Map<String, Object> selectBoardDetail(Map<String, Object> map) throws Exception{
		return (Map<String, Object>) selectOne("sample.selectBoardDetail", map);
	}					//"맴버네임스페이스("sample.select_id" = 쿼리ID, 오브젝트param=map) 라 부르는구나

	public void updateBoard(Map<String, Object> map) throws Exception{
		update("sample.updateBoard", map);
	}
	//딜리트로 네이밍되어있지만 업데이트 활용
	public void deleteBoard(Map<String, Object> map) throws Exception{
		update("sample.deleteBoard", map);
	}
	//바뀜3-7 추가
	public void insertFile(Map<String, Object> map) throws Exception{
		insert("sample.insertFile", map);
	}
	
	@SuppressWarnings("unchecked") // (4-4바뀜 신설)
	public List<Map<String, Object>> selectFileList(Map<String, Object> map) throws Exception{
		return (List<Map<String, Object>>)selectList("sample.selectFileList", map);
	}
	
}

/*참고 b1b1 위에 쓰인 메소드들은 SQL용 메소드들이다. AbstractDAO를 임포트함. (쿼리id, 오브젝트)
 * 														ㄴ 는 //마이바티스SqlSessionTemplate를 임포트 함.
//상속받는 업데이트 
public Object update(String queryId, Object params){
	printQueryId(queryId);
	return sqlSession.update(queryId, params);
}

//상속받는 인서트
	public Object insert(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.insert(queryId, params);
*/
