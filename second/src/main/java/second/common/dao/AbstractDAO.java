package second.common.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory; //커몬스
import org.mybatis.spring.SqlSessionTemplate; //마이바티스SqlSessionTemplate
import org.springframework.beans.factory.annotation.Autowired;

//(앱스트랙트DAO - 마이바티스로 활용하는 틀을 잡아줌) 
public class AbstractDAO {
	protected Log log = LogFactory.getLog(AbstractDAO.class);
	
	@Autowired    // 선언만했지만 오토와이어드로, 타입(빈쪽 class 내용을 살펴보자 )
	private SqlSessionTemplate sqlSession;
	
	// 디버그하는게 맞다면
	protected void printQueryId(String queryId) {
		if(log.isDebugEnabled()){ 
			log.debug("\t QueryId  \t:  " + queryId);
		}
	}
	
	// 매퍼방식 아닌 원론적인 방식 (쿼리메소드 기능을 활용) 
	//인서트
	public Object insert(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.insert(queryId, params);
	}	
	//업데이트
	public Object update(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.update(queryId, params);
	}
	//딜리트
	public Object delete(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.delete(queryId, params);
	}
	//셀렉트원 [참고 아이바티스 - 쿼리포오브젝트queryForObject() ]
	public Object selectOne(String queryId){
		printQueryId(queryId);
		return sqlSession.selectOne(queryId);
	}
	//셀렉트원 파라미터 2개인것
	public Object selectOne(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.selectOne(queryId, params);
	}
	//셀렉트리스트 [참고 아이바티스 - 쿼리포리스트 queryForList() ]
	@SuppressWarnings("rawtypes")
	public List selectList(String queryId){
		printQueryId(queryId);
		return sqlSession.selectList(queryId);
	}
	//셀렉트리스트 파라미터 2개인것
	@SuppressWarnings("rawtypes")
	public List selectList(String queryId, Object params){
		printQueryId(queryId);
		return sqlSession.selectList(queryId,params);
	}
}

