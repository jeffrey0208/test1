package second.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;  

@Component("fileUtils") // 자동 컨테이너에 빈 등록
public class FileUtils {
	private static final String filePath = "C:\\dev\\file\\";
	
	public List<Map<String,Object>> parseInsertFileInfo(Map<String,Object> map, HttpServletRequest request) throws Exception{
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
    	Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
    	
    	MultipartFile multipartFile = null;
    	String originalFileName = null;
    	String originalFileExtension = null;
    	String storedFileName = null;
    	
    	List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String, Object> listMap = null; 
        
        String boardIdx = (String)map.get("IDX");
        
        File file = new File(filePath);
        if(file.exists() == false){
        	file.mkdirs();  // 경로가 없으면 만들도록 한다.
        }
        
        while(iterator.hasNext()){
        	multipartFile = multipartHttpServletRequest.getFile(iterator.next());
        	if(multipartFile.isEmpty() == false){
        		originalFileName = multipartFile.getOriginalFilename();
        		originalFileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
        		storedFileName = CommonUtils.getRandomString() + originalFileExtension;
        		//ㄴ 외 과정이 있어서 중복네임이 있더라도 겹치지 않게 랜덤한 이름 32자를 뽑아넣는것.

        		file = new File(filePath + storedFileName);  //파일객체 생성하고
        		multipartFile.transferTo(file); //파일객체 넣어주면 , 저장한 이름 과 디렉토리에 넣어준다.
        		
        		listMap = new HashMap<String,Object>();  //해쉬맵 객체에다가 몽땅 세팅하고
        		listMap.put("BOARD_IDX", boardIdx);
        		listMap.put("ORIGINAL_FILE_NAME", originalFileName);
        		listMap.put("STORED_FILE_NAME", storedFileName);
        		listMap.put("FILE_SIZE", multipartFile.getSize());
        		list.add(listMap);  //다중업로드를 대비해서 리스트로 넣어준다.
        	} 
        }
		return list;
	}
}