package second.common.util;

import java.util.UUID;  

public class CommonUtils {
	
	public static String getRandomString(){
		return UUID.randomUUID().toString().replaceAll("-", ""); 
		//혹 랜덤문자열에 대시기호가 있으면 빼고 임시이름을 만들어줘라.
	}
}