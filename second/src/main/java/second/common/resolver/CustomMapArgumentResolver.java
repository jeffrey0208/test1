package second.common.resolver;

import java.util.Enumeration; //이너멀레이션

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import second.common.common.CommandMap; //커맨드맵을 검증하기위함

public class CustomMapArgumentResolver implements HandlerMethodArgumentResolver{

	@Override //사용가능한지 검증
	public boolean supportsParameter(MethodParameter parameter) {
		return CommandMap.class.isAssignableFrom(parameter.getParameterType());
	}

	@Override //저장작업
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		CommandMap commandMap = new CommandMap();  //저장을 위한 객체 만들고
		
		HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
		Enumeration<?> enumeration = request.getParameterNames();
		
		String key = null;
		String[] values = null;
		while(enumeration.hasMoreElements()) {
			key = (String) enumeration.nextElement();
			values = request.getParameterValues(key);
			if(values != null) {
				commandMap.put(key,(values.length > 1) ? values:values[0]);
			}//커맨드맵에 집어넣는 작업, 밸류는 삼항연산, 코드가 깔끔하다.
		}
		return commandMap;  //오브젝트에 커맨드맵을 리턴
	}

	
	
}
