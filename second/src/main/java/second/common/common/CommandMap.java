package second.common.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;//유틸맵이다
import java.util.Set;

public class CommandMap {

	/* 필드 */
	
	//맵객체 생성
	Map<String,Object> map = new HashMap<String,Object>();
	
	/*참고 : 메소드(반환o) / 생성자(클래스명과 동일,초기화,반환x) */
	
	//가져오는용도
	public Object get(String key) {
		return map.get(key);
	}
	//입력용도
    public void put(String key, Object value){
        map.put(key, value);
    }
    //제거용도
    public Object remove(String key){
        return map.remove(key);
    }
    //키확인용도
    public boolean containsKey(String key){
        return map.containsKey(key);
    }
    //밸류확인용도
    public boolean containsValue(Object value){
        return map.containsValue(value);
    }
    //초기화용도인듯
    public void clear(){
        map.clear();
    }
    //셋터즈 맵
    public Set<Entry<String, Object>> entrySet(){
        return map.entrySet();
    }
    //셋터즈 키
    public Set<String> keySet(){
        return map.keySet();
    }
    //비었는지 확인용도
    public boolean isEmpty(){
        return map.isEmpty();
    }
    //모두입력용도
    public void putAll(Map<? extends String, ?extends Object> m){
        map.putAll(m);
    }
    //맵으로 반환하는용도
    public Map<String,Object> getMap(){
        return map;
    }
    
}
/*
 * 왜 맵 or 해시맵이 아니라 커맨드 맵인가.? 
 * 그냥 맵과 해시맵은 전송된걸 받을수 없기때문에 커맨드 맵을 만들어 쓴다. 
 * 상속도 사용할 수 없음.
 * 
 * 다만 parameterType="hashmap" 에서 볼 수 있듯 DB 연동 작업할 땐 
 * 맵 타입이 필요하기 때문에  진짜맵을 보내줘야한다..!
 * 그래서 맨 하단 Map 객체를 getMap() 으로 리턴받아서 변환하는 과정을 활용함. 
 */