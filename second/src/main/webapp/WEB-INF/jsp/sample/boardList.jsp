<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/WEB-INF/include/include-header.jspf" %>  <!-- //인크루드 헤더 -->
</head>
<body>
    <h2>게시판 목록</h2>
    <table class="board_list">
        <colgroup>
            <col width="10%"/>
            <col width="*"/>
            <col width="15%"/>
            <col width="20%"/>
        </colgroup>
        <thead>
            <tr>
                <th scope="col">글번호</th>
                <th scope="col">제목</th>
                <th scope="col">조회수</th>
                <th scope="col">작성일</th>
            </tr>
        </thead><!-- 리스트 안에 몇개의 객체가 저장되어있는지 확인. -->
        	<!-- //리스트를-> 로우에 저장, 키값을 이용해 출력 -->
        <tbody>
            <c:choose>
                <c:when test="${fn:length(list) > 0}">
	
                    <c:forEach items="${list }" var="row">
                        <tr>
                            <td>${row.IDX }</td>
                            <td class="title">
                                <a href="#this" name="title">${row.TITLE }</a> <!--  //제목을 누르면 상세보기로 가는데, 샵디스에 큰 의미는 없다. -->
                                <input type="hidden" id="IDX" value="${row.IDX }">
                            </td>
                            <td>${row.HIT_CNT }</td>
                            <td>${row.CREA_DTM }</td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise> <!-- //없으면 -->
                    <tr>
                        <td colspan="4">조회된 결과가 없습니다.</td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
    <br/>
    
  <!--   //A태그인데 버튼처럼 보이게 나왓는데 CSS클래스로 해놓았다(블럭 만들어 출력) -->
     <a href="#this" class="btn" id="write">글쓰기</a> 
     
    <%@ include file="/WEB-INF/include/include-body.jspf" %><!--  //폼이 없으면 가져다 쓸 수있도록 -->
    <script type="text/javascript">
        $(document).ready(function(){  //제이쿼리 문법
            $("#write").on("click", function(e){ ///글쓰기 버튼 id가 write 인 것을 클릭하면 이것을 호출해라
                e.preventDefault();
                fn_openBoardWrite();  //-> 아래줄 가서보면 컴서브밋 객체만들고, setUrl 넣고 전송한다.
            }); 
             
            $("a[name='title']").on("click", function(e){ ///제목   a태그중에 네임이 타이틀인 것을 클릭하면 이것을 호출해라
                e.preventDefault();
                fn_openBoardDetail($(this));
            });
        });
         
         
        function fn_openBoardWrite(){
            var comSubmit = new ComSubmit();
            comSubmit.setUrl("<c:url value='/sample/openBoardWrite.do' />");
            comSubmit.submit();
        }
         
        function fn_openBoardDetail(obj){
            var comSubmit = new ComSubmit();  //객체 생성하고
            comSubmit.setUrl("<c:url value='/sample/openBoardDetail.do' />"); //url설정하고
            comSubmit.addParam("IDX", obj.parent().find("#IDX").val()); //값을 넘기는데, IDX 라는 이름으로 / #IDX 는 히든 폼으로 넣어둔 것
            comSubmit.submit();
        }
    </script> 
</body>
</html>