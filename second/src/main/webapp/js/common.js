///1 경우의수를 다 넣어서 null 이면 트루가 나오도록만드는 것
function gfn_isNull(str) {    
	if (str == null) return true;
	if (str == "NaN") return true;
	if (new String(str).valueOf() == "undefined") return true;    
    var chkStr = new String(str);
    if( chkStr.valueOf() == "undefined" ) return true;
    if (chkStr == null) return true;    
    if (chkStr.toString().length == 0 ) return true;   
    return false; 
}

///2 폼 id 를 넣는데 id 버튼, 잘쓰면 편하다. 컴서브밋, 참고로 실사용 때 인크루드 안해놓으면 작동안한다.
function ComSubmit(opt_formId) {  

///널인지 아닌지체크 , 널이면, 폼 id를 커먼폼으로 쓸거고  / 널이 아니면 전달받은걸 쓴다.
	this.formId = gfn_isNull(opt_formId) == true ? "commonForm" : opt_formId;
	this.url = "";
	
	if(this.formId == "commonForm"){
		$("#commonForm")[0].reset(); /// #의 의미는 id 값으로 커먼을 줬다는 의미. (리셋을 시켜라)
	}
	
	this.setUrl = function setUrl(url){ //세팅하고
		this.url = url;
	};
	
	this.addParam = function addParam(key, value){ //파람설정
		///값전송을 할건데, 죽어도 전송이 안되니 히든폼을 스크립트를 통해 만드는 것이구나.
		$("#"+this.formId).append($("<input type='hidden' name='"+key+"' id='"+key+"' value='"+value+"' >"));
	}; ///jspf 폼태그 사이에 어팬드 내용이 반영되도록
	
	this.submit = function submit(){
		var frm = $("#"+this.formId)[0];
		frm.action = this.url; //어디로 보낼지
		frm.method = "post"; //방식은 포스트
		frm.submit();	
	};
}